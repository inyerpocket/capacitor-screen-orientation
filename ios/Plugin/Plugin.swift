import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(ScreenOrientation)
public class ScreenOrientation: CAPPlugin, UIApplicationDelegate {
    
    var lastOrientation = 0
    
    @objc func lock(_ call: CAPPluginCall) {
        DispatchQueue.main.async {
            let orientation = call.getString("orientation") ?? "portrait"
            print("CALL LOCK - "+orientation)
    //        let currentOrientation = UIDevice.current.orientation
            let detectedLandscape = UIApplication.shared.statusBarOrientation.isLandscape
            var value: Int
            var mask: UIInterfaceOrientationMask
            if(detectedLandscape) {
                value = 1
            } else {
                value = 0
            }
            mask = UIInterfaceOrientationMask.portrait
            
            self.lastOrientation = UIDevice.current.orientation.rawValue
            
            switch orientation {
                case "portrait":
                    if(detectedLandscape) {
                        value = UIInterfaceOrientation.portrait.rawValue
                    }
                    mask = UIInterfaceOrientationMask.portrait
                    break
                case "landscape":
                    if(!detectedLandscape) {
                        value = UIInterfaceOrientation.landscapeLeft.rawValue
                    }
                    mask = UIInterfaceOrientationMask.landscape
                    break
                case "landscape-primary":
                    value = UIInterfaceOrientation.landscapeLeft.rawValue
                    mask = UIInterfaceOrientationMask.landscapeLeft
                    break
                case "portrait-primary":
                    value = UIInterfaceOrientation.portrait.rawValue
                    mask = UIInterfaceOrientationMask.portrait
                    break
                case "landscape-secondary":
                    value = UIInterfaceOrientation.landscapeRight.rawValue
                    mask = UIInterfaceOrientationMask.landscapeRight
                    break
                case "portrait-secondary":
                    value = UIInterfaceOrientation.portraitUpsideDown.rawValue
                    mask = UIInterfaceOrientationMask.portraitUpsideDown
                    break
                default:
                    print("UNSPECIFIED")
            }
                
            let orientationData:[String: UIInterfaceOrientationMask] = ["orientationMask": mask]
            NotificationCenter.default.post(name: Notification.Name("SetOrientationLock"), object: self, userInfo: orientationData)
            UIDevice.current.setValue(value, forKey: "orientation")
            UINavigationController.attemptRotationToDeviceOrientation()

            call.success()
        }
    }

    @objc func unlock(_ call: CAPPluginCall) {
        DispatchQueue.main.async {
            print("CALL UNLOCK")
            let orientationData:[String: UIInterfaceOrientationMask] = ["orientationMask": UIInterfaceOrientationMask.all]
            NotificationCenter.default.post(name: Notification.Name("SetOrientationLock"), object: self, userInfo: orientationData)
            
//            if(self.lastOrientation > 0) {
//                UIDevice.current.setValue(self.lastOrientation, forKey: "orientation")
//                UINavigationController.attemptRotationToDeviceOrientation()
//                self.lastOrientation = 0
//            }

            call.success()
        }
    }
}
