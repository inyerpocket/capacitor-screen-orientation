# Capacitor Screen Orientation

## Installation

* `npm i capacitor-screen-orientation`

### Android

Add `import com.iypsoftware.capacitor.ScreenOrientation.ScreenOrientation;` and `add(ScreenOrientation.class);` in the app's `MainActivity.java` like this:

```
import com.iypsoftware.capacitor.ScreenOrientation.ScreenOrientation;

public class MainActivity extends BridgeActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Initializes the Bridge
    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
      // Additional plugins you've installed go here
      // Ex: add(TotallyAwesomePlugin.class);
      add(ScreenOrientation.class);
    }});
  }
}
```

### iOS

You will need to add the following code to the app's `AppDelegate.swift` immediately under `var window: UIWindow?` like this:

```
var orientationLock = UIInterfaceOrientationMask.all

func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
  return self.orientationLock
}

@objc func setOrientationLock(_ notification: Notification) {
  if let data = notification.userInfo as? [String: UIInterfaceOrientationMask]
  {
      for(name,value) in data
      {
        self.orientationLock = value
      }
  }
}

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
  // Override point for customization after application launch.
  let nc = NotificationCenter.default
  nc.addObserver(self, selector: #selector(setOrientationLock), name: Notification.Name("SetOrientationLock"), object: nil)
  return true
}
```

## Usage

```ts
import { ScreenOrientation } from 'capacitor-screen-orientation';

await ScreenOrientation.lock({orientation: 'portrait'})
await ScreenOrientation.unlock()
```

## Supported Orientations

#### portrait-primary
> The orientation is in the primary portrait mode.

#### portrait-secondary
> The orientation is in the secondary portrait mode.

#### landscape-primary
> The orientation is in the primary landscape mode.

#### landscape-secondary
> The orientation is in the secondary landscape mode.

#### portrait
> The orientation is either portrait-primary or portrait-secondary (sensor).

#### landscape
> The orientation is either landscape-primary or landscape-secondary (sensor).

## Events

Both android and iOS will fire the orientationchange event on the window object.
For this version of the plugin use the window object if you require notification.


### Example usage

```js
window.addEventListener("orientationchange", function(){
    console.log(screen.orientation.type); // e.g. portrait
});
```
