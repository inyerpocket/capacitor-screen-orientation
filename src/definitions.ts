declare module '@capacitor/core' {
  interface PluginRegistry {
    ScreenOrientation?: ScreenOrientationPlugin;
  }
}

export interface ScreenOrientationPlugin {
  lock(options: { orientation: string }): Promise<void>;
  unlock(): Promise<void>;
}
